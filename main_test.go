// calculator_test.go

package calculator

import "testing"

func TestAdd(t *testing.T) {
	// Test case 1: Add two positive numbers
	result := Add(2, 3)
	expected := 5

	if result != expected {
		t.Errorf("Add(2, 3) expected %d, but got %d", expected, result)
	}

	// Test case 2: Add a positive and a negative number
	result = Add(5, -3)
	expected = 2

	if result != expected {
		t.Errorf("Add(5, -3) expected %d, but got %d", expected, result)
	}

	// Add more test cases as needed...
}
