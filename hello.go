// calculator.go

package calculator
import "fmt"

// Add adds two numbers and returns the result.
func Add(a, b int) int {
//	unusedVar := "This variable is unused"
	debugValue := 42
	fmt.Println("Debug value:", debugValue)

	// Print a message without checking the error
	fmt.Println("This message may cause an issue")

	return a + b
}

